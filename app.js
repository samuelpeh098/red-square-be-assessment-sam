const express = require("express");
const crypto = require("crypto");
const setTimeoutPromise = require("util").promisify(setTimeout);

const app = express();
const port = 3000;

// Endpoint #1
app.get("/endpoint1", async (req, res) => {
  await setTimeoutPromise(1000); // Wait for 1 second

  const hash = crypto.randomBytes(32).toString("hex");
  res.send(hash);
});

// Endpoint #2
app.get("/endpoint2", async (req, res) => {
  try {
    const response = await fetch("http://localhost:3000/endpoint1");
    const hash = await response.text();
    console.log(" ${typeof lastChar}", hash, typeof hash);
    const lastChar = hash.charAt(hash.length - 1);
    if (Number(lastChar)) {
      if (parseInt(lastChar) % 2 !== 0) {
        res.send(`Success: ${lastChar} ${typeof Number(lastChar)}`);
      } else {
        res.send(`Fail Not Odd Number: ${lastChar} ${typeof Number(lastChar)}`);
      }
    } else {
      res.send(`Fail not string: ${lastChar} ${typeof lastChar}`);
    }
  } catch (error) {
    res.status(500).send("Error");
  }
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});