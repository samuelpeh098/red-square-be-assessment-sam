# red-square-be-assessment-sam

## Getting started

Make sure npm is installed in your local PC

Run the following command to get started
```
npm i
npm run start
```
    

The API endpoint domain will be http://localhost:3000/,
There are 2 endpoints:

```
http://localhost:3000/endpoint1
http://localhost:3000/endpoint2
```

In order to test the endpoint2, run the following command:

```
loadtest -c 1 --rps 1 http://localhost:3000/endpoint2
```